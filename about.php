<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>BRPBATPP | IPB TEK54</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/animate.css">

	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="css/ionicons.min.css">
	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>

	<!-- Header -->
	<?php include 'component/header.php'; ?>
	<!-- Navigation -->
	<?php $menu = 'about'; ?>
	<?php include 'component/navigation.php'; ?>

	<section class="hero-wrap hero-wrap-2" style="background-image: url('gambar/gamten.jpg');" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row no-gutters slider-text align-items-end">
				<div class="col-md-9 ftco-animate pb-5">
					<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Tentang <i class="ion-ios-arrow-forward"></i></span></p>
					<h1 class="mb-0 bread">Tentang</h1>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section ftco-degree-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 ftco-animate">
					<!-- <h2 class="mb-3">It is a long established fact a reader be distracted</h2> -->
					<p>BRPBATPP (Balai Riset Perikanan Budidaya Air Tawar dan Penyuluhan Perikanan) merupakan sebuah lembaga dari Kementrian Kelautan dan Perikanan (KKP) yang bergerak di bidang Penelitian dan Pengembangan Perikanan Budidaya. Pada saat ini, BRPBATPP masih melakukan pemantau kelayakan air pada sirkulasi air Kolam pembibitan ikan secara manual yaitu  dengan menggunakan alat ukur suhu dan kadar kejernihan air secara berkala.</p>
					<p>Dengan adanya alat monitoring kelayakan air yang besifat IoT dapat mempermudah pegawai BRPBATPP dalam melakukan pengukuran kelayakan air kolam secara teratur dan efisien. Alat monitoring ini diperlukan karena selain halnya mempermudah dalam memantau kelayakan air kolam tersebut, alat monitoring ini juga dapat memantau sekaligus banyak kolam dan mencatat perubahan data nilai ukur ataupun informasi persetiap waktunya, oleh sebab itu keadaan suatu kolam dapat sangat diperhatikan, agar apabila kelayakan air pada Kolam tersebut sudah tidak bagus dan mengalami perubahan, pegawai dapat langsung mengetahuinya dan menangulangi apabila ada masalah terhadap sistem resirkulasi pada kolam pembibitan ikan tersebut</p>
					<br>
					<div class="about-author d-flex p-4 bg-light">
						<div class="bio mr-5">
							<img src="images/ramot-san.jpeg" alt="Image placeholder" style="height: 100%">
						</div>
						<div class="desc" style="padding-left: 0px">
							<h3 style="color: black"><strong>Ramot Mangiring Silaban</strong> </h3>
							<h3 style="color: black"> <strong>J3D117001</strong> </h3>
							<p>Ramot Mangiring Silaban, dilahirkan di Bandung, Jawa Barat pada tanggal 21  November 1999. merupakan anak keempat dari delapan bersaudara, diterima sebagai Mahasiswa di Institut Pertanian Bogor Program Diploma yang sekarang berubah menjadi Sekolah Vokasi dengan Program Studi Teknik Komputer angkatan 54 melalui jalur USMI dan pada Semeter 6  PKL Dibalai intansi BRPBATPP (Balai Riset Perikanan Budidaya Air Tawar dan Penyuluhan Perikanan) selama 45 Hari Kerja</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> <!-- .section -->

	<?php include 'component/footer.php'; ?>

	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
			<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
			<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg></div>


	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/jquery.animateNumber.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/scrollax.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
	<script src="js/google-map.js"></script>
	<script src="js/main.js"></script>

</body>

</html>