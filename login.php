<?php
require_once("config.php");
if (isset($_POST['login'])) {

	$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
	$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
	$sql = "SELECT * FROM users WHERE  email=:email";
	$stmt = $db->prepare($sql);
	// bind parameter ke query
	$params = array(
		":email" => $email
	);

	$stmt->execute($params);
	$user = $stmt->fetch(PDO::FETCH_ASSOC);
	
	// jika user terdaftar
	if ($user) {
		// verifikasi password
		if (password_verify($password, $user["password"])) {
			session_start();
			$_SESSION["user"] = $user;
			if($user['is_admin']){
				header("Location: dashboard.php");
			}else{
				header("Location: monitoring.php");
			}
			
		} else {
			header("Location: login.php?auth=failed");
		}
	} else {
		header("Location: login.php?auth=failed");
	}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>BRPBATPP | IPB TEK54</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="css/animate.css">

	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/magnific-popup.css">

	<link rel="stylesheet" href="css/ionicons.min.css">

	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="css/style.css">

	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--===============================================================================================-->
</head>

<body>
	<!-- Header -->
	<?php include 'component/header.php'; ?>
	<!-- Navigation -->
	<?php $menu = 'login'; ?>
	<?php include 'component/navigation.php'; ?>


	<section style="margin-bottom: 40px">
		<div class="container">
			<?php
			if (isset($_GET['auth']) == 'failed') { ?>
				<div class="row" style="padding: 20px;margin-bottom:-100px">
					<div class="col-sm-10 offset-sm-1">
						<div class="alert alert-danger">
							<strong>Login Failed!</strong> Wrong username or password.
						</div>
					</div>
				</div>
			<?php }
			?>
			<div class="row justify-content-center">
				<div class="wrap-login100" style="margin-left:60px;padding-top:80px;">
					<div class="login100-pic js-tilt" data-tilt>
					c	c<img src="images/img-01.png" alt="IMG">
					</div>
					<form class="login100-form validate-form" action="" method="POST">
						<span class="login100-form-title">
							<h2>Login</h2>
						</span>

						<div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
							<input class="input100" type="text" name="email" placeholder="Email" required>
							<span class="focus-input100"></span>
							<span class="symbol-input100">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</span>
						</div>

						<div class="wrap-input100 validate-input" data-validate="Password is required">
							<input class="input100" type="password" name="password" placeholder="Password" required>
							<span class="focus-input100"></span>
							<span class="symbol-input100">
								<i class="fa fa-lock" aria-hidden="true"></i>
							</span>
						</div>

						<div class="container-login100-form-btn">
							<input class="login100-form-btn" type="submit" name="login" value="Login">
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	<?php include 'component/footer.php'; ?>
	
	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
			<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
			<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg></div>


	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/jquery.animateNumber.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/scrollax.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
	<script src="js/google-map.js"></script>

	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
	<script>
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
	<!--========-->
	<script src="js/main.js"></script>

</body>

</html>