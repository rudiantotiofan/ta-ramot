<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>BRPBATPP | IPB TEK54</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="css/ionicons.min.css">
	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>

	<!-- Header -->
	<?php include 'component/header.php'; ?>
	<!-- Navigation -->
	<?php $menu = 'project'; ?>
	<?php include 'component/navigation.php'; ?>

	<section class="hero-wrap hero-wrap-2" style="background-image: url('gambar/gampro.jpg');" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row no-gutters slider-text align-items-end">
				<div class="col-md-9 ftco-animate pb-5">
					<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Projek <i class="ion-ios-arrow-forward"></i></span></p>
					<h1 class="mb-0 bread">Projek</h1>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section ftco-no-pt ftco-no-pb">
		<div class="container-fluid px-md-0">
			<div class="row no-gutters">

				<div class="col-md-4 ftco-animate">
					<div class="work img d-flex align-items-end" style="background-image: url(images/asset/img-01.jpeg);">
						<a href="images/asset/img-01.jpeg" class="icon image-popup d-flex justify-content-center align-items-center">
							<span class="icon-expand"></span>
						</a>
						<div class="desc w-100 px-4">
							<div class="text w-100 mb-3">
								<span>Building</span>
								<h2><a href="work-single.html">College Health Profession</a></h2>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 ftco-animate">
					<div class="work img d-flex align-items-end" style="background-image: url(images/asset/img-02.jpeg);">
						<a href="images/asset/img-02.jpeg" class="icon image-popup d-flex justify-content-center align-items-center">
							<span class="icon-expand"></span>
						</a>
						<div class="desc w-100 px-4">
							<div class="text w-100 mb-3">
								<span>Building</span>
								<h2><a href="work-single.html">College Health Profession</a></h2>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 ftco-animate">
					<div class="work img d-flex align-items-end" style="background-image: url(images/asset/img-03.jpeg);">
						<a href="images/asset/img-03.jpeg" class="icon image-popup d-flex justify-content-center align-items-center">
							<span class="icon-expand"></span>
						</a>
						<div class="desc w-100 px-4">
							<div class="text w-100 mb-3">
								<span>Building</span>
								<h2><a href="work-single.html">College Health Profession</a></h2>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 ftco-animate">
					<div class="work img d-flex align-items-end" style="background-image: url(images/asset/img-04.jpeg);">
						<a href="images/asset/img-04.jpeg" class="icon image-popup d-flex justify-content-center align-items-center">
							<span class="icon-expand"></span>
						</a>
						<div class="desc w-100 px-4">
							<div class="text w-100 mb-3">
								<span>Building</span>
								<h2><a href="work-single.html">College Health Profession</a></h2>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 ftco-animate">
					<div class="work img d-flex align-items-end" style="background-image: url(images/asset/img-05.jpeg);">
						<a href="images/asset/img-05.jpeg" class="icon image-popup d-flex justify-content-center align-items-center">
							<span class="icon-expand"></span>
						</a>
						<div class="desc w-100 px-4">
							<div class="text w-100 mb-3">
								<span>Building</span>
								<h2><a href="work-single.html">College Health Profession</a></h2>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 ftco-animate">
					<div class="work img d-flex align-items-end" style="background-image: url(images/asset/img-06.jpeg);">
						<a href="images/asset/img-06.jpeg" class="icon image-popup d-flex justify-content-center align-items-center">
							<span class="icon-expand"></span>
						</a>
						<div class="desc w-100 px-4">
							<div class="text w-100 mb-3">
								<span>Building</span>
								<h2><a href="work-single.html">College Health Profession</a></h2>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 ftco-animate">
					<div class="work img d-flex align-items-end" style="background-image: url(images/asset/img-07.jpeg);">
						<a href="images/asset/img-07.jpeg" class="icon image-popup d-flex justify-content-center align-items-center">
							<span class="icon-expand"></span>
						</a>
						<div class="desc w-100 px-4">
							<div class="text w-100 mb-3">
								<span>Building</span>
								<h2><a href="work-single.html">College Health Profession</a></h2>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 ftco-animate">
					<div class="work img d-flex align-items-end" style="background-image: url(images/asset/img-08.jpeg);">
						<a href="images/asset/img-08.jpeg" class="icon image-popup d-flex justify-content-center align-items-center">
							<span class="icon-expand"></span>
						</a>
						<div class="desc w-100 px-4">
							<div class="text w-100 mb-3">
								<span>Building</span>
								<h2><a href="work-single.html">College Health Profession</a></h2>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 ftco-animate">
					<div class="work img d-flex align-items-end" style="background-image: url(images/asset/img-09.jpeg);">
						<a href="images/asset/img-09.jpeg" class="icon image-popup d-flex justify-content-center align-items-center">
							<span class="icon-expand"></span>
						</a>
						<div class="desc w-100 px-4">
							<div class="text w-100 mb-3">
								<span>Building</span>
								<h2><a href="work-single.html">College Health Profession</a></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include 'component/footer.php'; ?>

	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
			<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
			<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg></div>


	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/jquery.animateNumber.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/scrollax.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
	<script src="js/google-map.js"></script>
	<script src="js/main.js"></script>

</body>

</html>