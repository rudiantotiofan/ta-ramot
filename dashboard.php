<?php
session_start();
require_once("auth.php");
include "adduser.php";
// include "delete.php"
include 'datatable.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>BRPBATPP | IPB TEK54</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="css/ionicons.min.css">
	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>

	<!-- Header -->
	<?php include 'component/header.php'; ?>
	<!-- Navigation -->
	<?php $menu = 'dashboard'; ?>
	<?php include 'component/navigation.php'; ?>

	<section class="hero-wrap hero-wrap-2" style="background-image: url('gambar/gamlog.jpg');" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row no-gutters slider-text align-items-end">
				<div class="col-md-9 ftco-animate pb-5">
					<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Dashboard <i class="ion-ios-arrow-forward"></i></span></p>
					<h1 class="mb-0 bread">Dashboard Admin</h1>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section">
		<div class="container">
			<?php
			if (isset($result) && $message != '-') {
				if ($message == 'success') {
					echo '<div class="alert alert-success" role="alert"> User berhasil ditambah ! </div>';
				} else {
					echo '<div class="alert alert-danger" role="alert"> User gagal ditambah ! </div>';
				}
			} else if (isset($result) && $delete != '-') {
				if ($delete == 'success') {
					echo '<div class="alert alert-info" role="alert"> User berhasil dihapus ! </div>';
				} else {
					echo '<div class="alert alert-danger" role="alert"> User gagal dihapus ! </div>';
				}

			}


			?>

			<div class="row justify-content-center">
				<button class="btn btn-info col-md-6 hide" type="button" id="btn-user">
					Management User
				</button>
			<!-- 	<button class="btn btn-info col-md-6 hide" type="button" id="btn-monitoring">
					Management Data Monitoring
				</button> -->

				<!-- colapse panel -->
				<div class="row">
					<div class="collapse" id="userpanel" style="display: none;">
						<form class="form-inline" id="form-user" action="" method="POST">
							<div class="form-group mb-2">
								<label for="staticEmail2" class="sr-only">Email</label>
								<input type="text" name="email" class="form-control-plaintext" id="staticEmail2" placeholder="email" required="true" style="border-width: 1px; border-color: gray">
							</div>
							<div class="form-group mx-sm-3 mb-2">
								<label for="inputPassword2" class="sr-only">Default Password</label>
								<input type="text" name="password" class="form-control-plaintext" id="inputPassword2" placeholder="password" required="true" style="border-width: 1px; border-color: gray">
							</div>
							<input type="submit" name="submit" class="btn btn-primary mb-2" value="Create" >
							<!-- <button id="btn-create" class="btn btn-primary"> Create </button> -->
						</form>
					</div>
					<!-- <div class="collapse" id="monitoringpanel" style="display: none;">
						<center>
							<a  class="btn btn-primary" target="_blank" href="export.php">EXPORT KE EXCEL</a>
						</center>
					</div> -->
				</div>
			</div>

				<div>
					<table id="example" class="table table-striped table-bordered" style="width:100% \" >
					        <thead>
					            <tr>
					                <th class="text-center">Email</th>
					                <th class="text-center">Rawpassword</th>
					                <th class="text-center">Action</th>
					                
					            </tr>
					        </thead>
					        <tbody>
					        	<?php
					        		foreach ($user as $data) {
								?>
					        	<tr>
					        		<td class="text-center"><?php echo $data['email']; ?> </td>
					        		<td class="text-center"><?php echo $data['raw_pass'];?> </td>
					  
					        		<td class="text-center">
					        			<button class="btn btn-danger delete" data-toggle="modal" data-id="<?php echo $data['id'];?>" data-target="#exampleModal">
					        				<span class="fa fa-trash-o" > Hapus </span>
					        			</button>
					        		</td>
					        	</tr>
					        	<?php } ?>
					        </tbody>
					</table>	
				</div>
				
	</section>

	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Apakah Anda yakin ingin menghapus user ini ?
	      </div>
	      <form id="form-delete" method="POST">
	      	<input type="hidden" name="id-delete" value ="" id="id-delete">
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
	        <button type="submit" name="delete" class="btn btn-info deleteok">Hapus</button>
	      </div>
	      </form>
	    </div>
	  </div>
	</div>

	<?php include 'component/footer.php'; ?>

	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
			<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
			<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg></div>


	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {exampleModal
		    $('#example').DataTable();
		} );

		$(document).on('click', '.delete',function(){
			var id = $(this).data('id');
			$("#exampleModal").find('.deleteok').data('id',id);
			$("#id-delete").val(id);
		});

		$(document).on('click', '.deleteok',function(){
			var id = $(this).data('id');
			
			$('#form-delete').submit();
	
		});

		// $(document).on("click", "#btn-create", function(){
		// 	let email = $("#staticEmail2").value();
		// 	let pass =	$("#inputPassword2").value();
		// 	if ( email !== '' && pass !== '' ){
		// 		$("#form-user").submit();
		// 	} else {
		// 		alert("Mohon pastikan email dan password sudah terisi !!");
		// 	}
		// });

		$(document).on('click', '#btn-user', function() {
			let btn_user = $(this);
			var status  = btn_user.hasClass("hide");
			if ( status ) {
				var check = $("#btn-monitoring").hasClass("show");
				if ( check ) {
						$("#btn-monitoring").removeClass("show").addClass("hide");
						$("#monitoringpanel").hide();	
				}
				btn_user.removeClass("hide").addClass("show");
				$("#userpanel").show();
			} else {
				btn_user.removeClass("show").addClass("hide");
				$("#userpanel").hide();	
			}
		});

		$(document).on('click', '#btn-monitoring', function() {
			let btn_monitoring = $(this);
			var status  = btn_monitoring.hasClass("hide");
			if ( status ) {
				var check = $("#btn-user").hasClass("show");
				if ( check ) {
						$("#btn-user").removeClass("show").addClass("hide");
						$("#userpanel").hide();	
				}
				btn_monitoring.removeClass("hide").addClass("show");
				$("#monitoringpanel").show();
			} else {
				btn_monitoring.removeClass("show").addClass("hide");
				$("#monitoringpanel").hide();	
			}
		});
	</script>

	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/jquery.animateNumber.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/scrollax.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
	<script src="js/google-map.js"></script>
	<script src="js/main.js"></script>

