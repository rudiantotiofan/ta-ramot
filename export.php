<?php
error_reporting(E_ERROR | E_PARSE);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Export Data Ke Excel</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;
 
	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>
 
	<?php

	header("Content-type: application/vnd-ms-excel");
	header('Pragma: public');       // required
	header('Expires: 0');           // no cache
	header("Content-Disposition: attachment; filename=Data_Monitoring.xls");
	header('Connection: close');

	ob_clean();
	flush();
		
	?>
 
	<center>
		<h1>Data Monitoring</h1>
	</center>
 
	<table border="1">
		<tr>
			<th>Tanggal</th>
			<th>Suhu</th>
			<th>Kekeluhan</th>
		</tr>
		<?php 
		
		// koneksi database
		include('config.php');

		// menampilkan data pegawai
			$sql = "SELECT * FROM monitoring";
			$stmt = $db->prepare($sql);
			$stmt->execute();
			$user = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if($user){
			foreach ($user as $value) {
			# code...
		?>
		<tr>
			<td><?php echo date('Y-m-d',strtotime($value['date'])); ?></td>
			<td><?php echo $value['suhu']; ?></td>
			<td><?php echo $value['kekeruhan']; ?></td>
		</tr>
		<?php 
			}
		}
		?>
	</table>
</body>
</html>