<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>BRPBATPP | IPB TEK54</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="css/ionicons.min.css">

	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>
	<!-- Header -->
	<?php include 'component/header.php'; ?>
	<!-- Navigation -->
	<?php $menu = 'home'; ?>
	<?php include 'component/navigation.php'; ?>
	<!-- Section Head -->
	<!-- END nav -->
	<div class="hero-wrap">
		<div class="home-slider owl-carousel">
			<div class="slider-item" style="background-image:url(gambar/gam1.jpg);">
				<div class="overlay"></div>
				<div class="container">
					<div class="row no-gutters slider-text align-items-center justify-content-center">
						<div class="col-md-12 ftco-animate">
							<div class="text w-100 text-center">
								<h1>Selamat Datang Di Website Kami</h1>
								<h2 class="mb-3">Website ini dibuat dengan tujuan untuk memonitoring air pada resirkulasi air kolam pembibitan ikan yang berada di lab GH secara realtime dan terdata dimana alat ini terintegrasi dengan web secara realtime.</h2>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="slider-item" style="background-image:url(gambar/gam3.jpg);">
				<div class="overlay"></div>
				<div class="container">
					<div class="row no-gutters slider-text align-items-center justify-content-center">
						<div class="col-md-12 ftco-animate">
							<div class="text w-100 text-center">
								<h1>Monitoring menjadi lebih mudah !</h1>
								<h2 class="mb-3">Dengan menggunakan teknologi IoT pada rancang bangun alat monitoring kelayakan air pada resirkulasi kolam pembibitan ikan, memantau dan mendata kondisi kelayakan pada air kolam ikan menjadi semakin lebih mudah dan efisien, dimana saja dan kapan saja.</h2>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="slider-item" style="background-image:url(gambar/gam2.jpg);">
				<div class="overlay"></div>
				<div class="container">
					<div class="row no-gutters slider-text align-items-center justify-content-center">
						<div class="col-md-12 ftco-animate">
							<div class="text w-100 text-center">
								<h1>Mengapa perlu Monitoring secara otomatis ?</h1>
								<h2 class="mb-3">Dalam pembibitan ataupun pemeliharaannya ikan diperlukan sebuah kondisi lingkungan air yang cocok dengan ikan tersebut sepertihalnya agar dapar bertahan hidup, seperti halnya temperatur ataupun kejernihan air, oleh karena itu penting untuk memonitoring kondisi lingkungan air kolam ikan tersebut.</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="ftco-section ftco-no-pt ftco-no-pb" style="margin-top: 25px">
		<div class="container">
			<div class="row d-flex no-gutters">
				<div class="col-md-6 d-flex">
					<div class="img img-video d-flex align-self-stretch align-items-center justify-content-center justify-content-md-end" style="background-image:url(images/iot.png);">
						<a href="https://www.youtube.com/watch?v=SDNimNIsfYQ" class="icon-video popup-vimeo d-flex justify-content-center align-items-center">
							<span class="icon-play"></span>
						</a>
					</div>
				</div>
				<div class="col-md-6 pl-md-5" style="min-height: 800px">
					<div class="row justify-content-start py-5">
						<div class="col-md-12 heading-section ftco-animate pl-md-4 py-md-4">
							<span class="subheading">Welcome to Balai Riset Perikanan</span>
							<h2 class="mb-4">We create and turn into reality</h2>
							<div class="tabulation-2 mt-4">
								<ul class="nav nav-pills nav-fill d-md-flex d-block">
									<li class="nav-item mb-md-0 mb-2">
										<a class="nav-link active py-2" data-toggle="tab" href="#home1"> BRPBATPP</a>
									</li>
									<li class="nav-item px-lg-2 mb-md-0 mb-2">
										<a class="nav-link py-2" data-toggle="tab" href="#home2">Sekolah Vokasi IPB</a>
									</li>
									<li class="nav-item">
										<a class="nav-link py-2 mb-md-0 mb-2" data-toggle="tab" href="#home3">Program Studi</a>
									</li>
								</ul>
								<div class="tab-content bg-light rounded mt-2">
									<div class="tab-pane container p-0 active" id="home1">
										<p>Balai Riset Perikanan Budidaya Air Tawar dan Penyuluhan Perikanan (BRPBATPP) merupakan sebuah lembaga yang membidangi penelitian dan pengembangan budidaya perikanan yang di mulai sejak pada masa kolonial Belanda yang diberi nama “Visserij Laboratorium Te Batavia” yang memiliki arti “Laboratorium Perikanan Di Batavia” dibentuk pada tahun 1905 dan bertugas untuk melakukan penelitian Biologi Perikanan.</p>
									</div>
									<div class="tab-pane container p-0 fade" id="home2">
										<p>Sekolah Vokasi IPB (SV-IPB) pertama kali dibuka pada tahun 1979 dengan nama Program Diploma IPB. Pada tahun 1980 program tersebut berubah nama menjadi Fakultas Politeknik Pertanian. Berdasarkan Peraturan Pemerintah No. 30 Tahun 1990 Fakultas Politeknik Pertanian ditiadakan dan program-program diploma yang berada dibawahnya harus diintegrasikan dalam fakultas yang relevan. Selanjutnya sejak tahun 1992 Program Studi Pendidikan Diploma di IPB dikelola oleh jurusan atau fakultas yang ada di lingkungan IPB. pada saat ini Sekolah Vokasi IPB memiliki 17 Program Studi. Selain menyelenggarakan program pendidikan bergelar</p>
									</div>
									<div class="tab-pane container p-0 fade" id="home3">
										<p>Program studi Teknik Komputer merupkan program studi unggulan dan terdepan dalam mencetak tenaga ahli profesional pada bidang rekayasa perangkat keras berbasis komputer, teknologi jaringan komputer, dan pemeliharaan sistem komputer yang berjiwa kewirausahaan, inovatif, adaptif, berdaya saing global, serta berakhlak mulia.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section" style="padding-bottom:0px">
		<div class="container">
			<div class="row justify-content-center mb-4 pb-1">
				<div class="col-md-7 heading-section text-center ftco-animate">
					<span class="subheading">Hard Work & Smart Work</span>
					<h2>Our Gallery</h2>
				</div>
			</div>
			<div class="row d-flex">
				<div class="col-md-4 d-flex ftco-animate">
					<div class="blog-entry align-self-stretch" style="width: 100%">
						<a href="#" class="block-20 rounded" style="background-image: url('images/balai-ipb.jpeg');">
						</a>
						<div class="text mt-3 text-center">
							<h3 class="heading"><a href="#">Balai Riset Perikanan Budidaya Air Tawar</a></h3>
						</div>
					</div>
				</div>
				<div class="col-md-4 d-flex ftco-animate">
					<div class="blog-entry align-self-stretch" style="width: 100%">
						<a href="#" class="block-20 rounded" style="background-image: url('images/vokasi-ipb.jpeg');">
						</a>
						<div class="text mt-3 text-center">
							<h3 class="heading"><a href="#">Sekolah Vokasi IPB (SV-IPB)</a></h3>
						</div>
					</div>
				</div>
				<div class="col-md-4 d-flex ftco-animate">
					<div class="blog-entry align-self-stretch" style="width: 100%">
						<a href="#" class="block-20 rounded" style="background-image: url('images/ramot-ipb.jpeg');">
						</a>
						<div class="text mt-3 text-center">
							<h3 class="heading"><a href="#">Program studi Teknik Komputer</a></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include 'component/footer.php'; ?>

	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
			<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
			<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg></div>


	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/jquery.animateNumber.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/scrollax.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
	<script src="js/google-map.js"></script>
	<script src="js/main.js"></script>

</body>

</html>