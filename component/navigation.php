<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	<div class="container">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="fa fa-bars"></span> Menu
		</button>
		<div class="social-media order-lg-last">
			<p class="mb-0 d-flex">
				<style>
					.social-icon {
						background-color: white;
					}
				</style>
				<a href="#" class="d-flex align-items-center justify-content-center social-icon"><span class="fa fa-facebook"><i class="sr-only">Facebook</i></span></a>
				<a href="#" class="d-flex align-items-center justify-content-center social-icon"><span class="fa fa-twitter"><i class="sr-only">Twitter</i></span></a>
				<a href="#" class="d-flex align-items-center justify-content-center social-icon"><span class="fa fa-instagram"><i class="sr-only">Instagram</i></span></a>
				<a href="#" class="d-flex align-items-center justify-content-center social-icon"><span class="fa fa-dribbble"><i class="sr-only">Dribbble</i></span></a>
			</p>
		</div>
		<div class="collapse navbar-collapse" id="ftco-nav">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item <?php echo $menu ==  'home' ? 'active' :'' ?>">
					<a href="index.php" class="nav-link">Home</a>
				</li>
				<li class="nav-item <?php echo $menu ==  'project' ? 'active' :'' ?>">
					<a href="project.php" class="nav-link">Project</a>
				</li>
				<!-- <li class="nav-item <?php echo $menu ==  'tutorial' ? 'active' :'' ?>">
					<a href="tutorial.php" class="nav-link">Tutorial</a>
				</li> -->
				<?php
				if ($login) {
					if ($admin == 1) { ?>
						<li class="nav-item <?php echo $menu ==  'dashboard' ? 'active' :'' ?>">
							<a href="dashboard.php" class="nav-link">Dasboard</a>
						</li>
					<?php } ?>
					<li class="nav-item <?php echo $menu ==  'monitoring' ? 'active' :'' ?>">
							<a href="monitoring.php" class="nav-link">Data Monitoring</a>
						</li>
					<li class="nav-item <?php echo $menu ==  'about' ? 'active' :'' ?>">
						<a href="about.php" class="nav-link">Tentang</a>
					</li>
					<li class="nav-item">
						<a href="logout.php" class="nav-link">Logout</a>
					</li>
				<?php
				} else { ?>
					<li class="nav-item <?php echo $menu ==  'about' ? 'active' :'' ?>">
						<a href="about.php" class="nav-link">Tentang</a>
					</li>
					<li class="nav-item <?php echo $menu ==  'login' ? 'active' :'' ?>">
						<a href="login.php" class="nav-link">Login</a>
					</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</nav>