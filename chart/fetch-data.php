<?php 
require_once("../config.php");
$sql = "SELECT date, suhu, kekeruhan FROM monitoring order by id desc limit 20 ";
$stmt = $db->prepare($sql);

$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
if ($result) {
    echo json_encode($result);
} else {
    echo json_encode(null);
}
?>