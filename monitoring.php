<?php
session_start();
require_once("auth.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>BRPBATPP | IPB TEK54</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/magnific-popup.css">

	<link rel="stylesheet" href="css/ionicons.min.css">
	<link rel="stylesheet" href="chart/Chart.min.css">
	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="css/style.css">
	<style>
		/* If you want you can use font-face */

		@font-face {
			font-family: 'BebasNeueRegular';
			src: url('BebasNeue-webfont.eot');
			src: url('BebasNeue-webfont.eot?#iefix') format('embedded-opentype'),
				url('BebasNeue-webfont.woff') format('woff'),
				url('BebasNeue-webfont.ttf') format('truetype'),
				url('BebasNeue-webfont.svg#BebasNeueRegular') format('svg');
			font-weight: normal;
			font-style: normal;
		}

		.clock {
			width: 100%;
			margin: 0 auto;
			border: 1px solid #333;
			color: #fff;
		}

		.clock>#Date {
			font-family: 'BebasNeueRegular', Arial, Helvetica, sans-serif;
			font-size: 26px;
			text-align: center;
			text-shadow: 0 0 5px #00c6ff;
		}

		.clock>ul {
			/* width: 800px; */
			margin: 0 auto;
			padding: 0px;
			list-style: none;
			text-align: center;
		}

		.clock>ul li {
			display: inline;
			font-size: 3em;
			text-align: center;
			font-family: 'BebasNeueRegular', Arial, Helvetica, sans-serif;
			text-shadow: 0 0 5px #00c6ff;
		}

		.clock>#point {
			position: relative;
			-moz-animation: mymove 1s ease infinite;
			-webkit-animation: mymove 1s ease infinite;
			padding-left: 10px;
			padding-right: 10px;
		}

		/* Simple Animation */
		@-webkit-keyframes mymove {
			0% {
				opacity: 1.0;
				text-shadow: 0 0 20px #00c6ff;
			}

			50% {
				opacity: 0;
				text-shadow: none;
			}

			100% {
				opacity: 1.0;
				text-shadow: 0 0 20px #00c6ff;
			}
		}

		@-moz-keyframes mymove {
			0% {
				opacity: 1.0;
				text-shadow: 0 0 20px #00c6ff;
			}

			50% {
				opacity: 0;
				text-shadow: none;
			}

			100% {
				opacity: 1.0;
				text-shadow: 0 0 20px #00c6ff;
			}

			;
		}
	</style>
</head>

<body>
	<!-- Header -->
	<?php include 'component/header.php'; ?>
	<!-- Navigation -->
	<?php $menu = 'monitoring'; ?>
	<?php include 'component/navigation.php'; ?>
	<!-- Section Head -->
	<section class="hero-wrap hero-wrap-2" style="background-image: url('gambar/gamor.jpg');" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row no-gutters slider-text align-items-end">
				<div class="col-md-9 ftco-animate pb-5">
					<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Data Monitoring <i class="ion-ios-arrow-forward"></i></span></p>
					<h1 class="mb-0 bread">Data Monitoring</h1>
				</div>
			</div>
		</div>
	</section>
	<!-- Section Content -->
	<section class="ftco-section">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h5 class="font-weight-bold">Monitoring Data</h5>
					<p>Sistem alat monitoring kelayakan air pada resirkulasi air kolam pembibitan ikan ini merupakan sebuah sistem yang  otomatis dan berbasis Internet of things ( IoT ) yang dapat membantu dalam memantau kelayakan air pada setiap kolam pembibitan ikannya secara realtime, dimana nilai yang ditampilkan pada grafik merupakan nilai yang diperoleh langsung dari data yang telah diukur oleh alat monitoring kelayakan air pada resirkulasi air kolam pembibitan ikan</p>
				</div>
				<div class="col-md-4">
					<h5 class="font-weight-bold">Current Time</h5>
					<div class="clock" style="background-color: black">
						<div id="Date"></div>
						<ul>
							<li id="hours"></li>
							<li id="point">:</li>
							<li id="min"></li>
							<li id="point">:</li>
							<li id="sec"></li>
						</ul>
					</div>
				</div>
			</div>
			<div style="padding-top:50px">
				<div class="row">
					<div class="col-sm-6" align="center">
						<canvas id="turbidityChart" width="400" height="250"></canvas><br>
						<div class="card text-center" style="width: 50%">
							<div class="card-body text-center">
								<span style="font-size: 28px;">
									<strong><span id="ntu"></span></strong> NTU
								</span><br>
								<span>Category: <span id="ntu-category"></span></span>
							</div>
						</div>
					</div>
					<div class="col-sm-6" align="center">
						<canvas id="temperatureChart" width="400" height="250"></canvas><br>
						<div class="card text-center" style="width: 50%">
							<div class="card-body text-center">
								<span style="font-size: 28px;">
									<strong><span id="celcius"></span></strong> ℃
								</span><br>
								<span>Category: <span id="celcius-category"></span></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include 'component/footer.php'; ?>


	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
			<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
			<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg></div>
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/jquery.animateNumber.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/scrollax.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
	<script src="js/google-map.js"></script>
	<script src="js/main.js"></script>
	<script src="chart/Chart.min.js"></script>
	<script>
		function fetchData() {
			var jqXHR = $.ajax({
				async: false,
				url: "chart/fetch-data.php",
				success: function(result) {
					return result;
				}
			});
			return JSON.parse(jqXHR.responseText);
		}
		// init first data
		var dataset = this.fetchData();
		var date = new Date();
		var time = [];
		var temperature = [];
		var turbidity = [];
		var ntu = '-';
		var celcius = '-';
		if (dataset) {
			dataset = dataset.reverse();
			dataset.map(res => {
				var dt = new Date(res.date);
				date = new Date(res.date);
				time.push(dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds());
				temperature.push(res.suhu);
				turbidity.push(res.kekeruhan);
				$('#celcius').text(res.suhu);
				$('#ntu').text(res.kekeruhan);
				ntu = res.kekeruhan ? (res.kekeruhan >= 0 && res.kekeruhan <= 10) ? 'Layak' : 'Tidak Layak' : '-';
				celcius = res.suhu ? (res.suhu >= 25 && res.suhu <= 30) ? 'Layak' : 'Tidak Layak' : '-';
			});
			$('#ntu-category').text(ntu);
			$('#celcius-category').text(celcius);
		}

		var temperatureCanvas = document.getElementById('temperatureChart');
		var turbidityCanvas = document.getElementById('turbidityChart');
		var turbidityData = {
			labels: time,
			datasets: [{
				label: "Data Kekeruhan - Last: " + date,
				fill: false,
				lineTension: 0.0,
				backgroundColor: "rgba(117, 192, 0,0.4)",
				borderColor: "rgba(117, 192, 0,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(117, 192, 0,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(117, 192, 0,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 5,
				pointHitRadius: 10,
				data: turbidity,
				fill: true,
			}]
		};

		var temperatureData = {
			labels: time,
			datasets: [{
				label: "Data Suhu - Last: " + date,
				fill: false,
				lineTension: 0.0,
				backgroundColor: "rgba(75,192,192,0.4)",
				borderColor: "rgba(75,192,192,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(75,192,192,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(75,192,192,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 5,
				pointHitRadius: 10,
				data: temperature,
				fill: true,
			}]
		};

		function adddata() {
			dataset = this.fetchData();
			time = [];
			temperature = [];
			turbidity = [];
			if (dataset) {
				dataset = dataset.reverse();
				dataset.map(res => {
					var dt = new Date(res.date);
					date = new Date(res.date);
					time.push(dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds());
					temperature.push(res.suhu);
					turbidity.push(res.kekeruhan);
					$('#celcius').text(res.suhu);
					$('#ntu').text(res.kekeruhan);
					ntu = res.kekeruhan ? (res.kekeruhan >= 0 && res.kekeruhan <= 50) ? 'Layak' : 'Tidak Layak' : '-';
					celcius = res.suhu ? (res.suhu >= 28 && res.suhu <= 34) ? 'Layak' : 'Tidak Layak' : '-';
				});
				$('#ntu-category').text(ntu);
				$('#celcius-category').text(celcius);
			}
			myTemperatureLineChart.data.labels = time;
			myTemperatureLineChart.data.datasets[0].data = temperature;
			myTemperatureLineChart.update();

			myTurbidityLineChart.data.labels = time;
			myTurbidityLineChart.data.datasets[0].data = turbidity;
			myTurbidityLineChart.update();
		}

		setInterval(function() {
			adddata();
		}, 4000);

		var option = {
			showLines: true
		};
		var myTemperatureLineChart = Chart.Line(temperatureCanvas, {
			data: temperatureData,
			options: option
		});
		var myTurbidityLineChart = Chart.Line(turbidityCanvas, {
			data: turbidityData,
			options: option
		});
	</script>

	<script>
		// Create two variable with the names of the months and days in an array
		var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

		// Create a newDate() object
		var newDate = new Date();
		// Extract the current date from Date object
		newDate.setDate(newDate.getDate());
		// Output the day, date, month and year   
		$('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

		setInterval(function() {
			// Create a newDate() object and extract the seconds of the current time on the visitor's
			var seconds = new Date().getSeconds();
			// Add a leading zero to seconds value
			$("#sec").html((seconds < 10 ? "0" : "") + seconds);
		}, 1000);

		setInterval(function() {
			// Create a newDate() object and extract the minutes of the current time on the visitor's
			var minutes = new Date().getMinutes();
			// Add a leading zero to the minutes value
			$("#min").html((minutes < 10 ? "0" : "") + minutes);
		}, 1000);

		setInterval(function() {
			// Create a newDate() object and extract tdevelophe hours of the current time on the visitor's
			var hours = new Date().getHours();
			// Add a leading zero to the hours value
			$("#hours").html((hours < 10 ? "0" : "") + hours);
		}, 1000);
	</script>
</body>

</html>