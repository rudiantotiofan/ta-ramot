<?php 
    $message = '-';
    if(isset($_POST['submit'])){
        require_once("config.php");
        $email      = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
        $password   = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
        $rawpass    = $password;

        $sql    = "INSERT INTO users (email,password,raw_pass) VALUES (:email,:password,:rawpass)";
        $stmt   = $db->prepare($sql);
        $params = array(
               ":email"     => $email,
               ":password"  => password_hash($password, PASSWORD_DEFAULT),
               ":rawpass"   => $rawpass,
           );
        $result = $stmt->execute($params);
        
            if($result){
                $message ='success';
               
            }else{
                $message ='failed';
            }     
    }


    if(isset($_POST['delete'])){
        require_once("config.php");
       
         $id      = $_POST['id-delete'];
        $sql    = "DELETE FROM users WHERE id=:id";
        $stmt   = $db->prepare($sql);
        $params = array(
               ":id"     => $id,
           );
        $result = $stmt->execute($params);
        if($result){
             $delete ='success';
        }else{
             $delete ='failed';
        }
    }
?>